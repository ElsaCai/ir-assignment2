#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg  import LaserScan
import numpy as np
import time


def move(s,s2):
        print('in move func')
		
	base_data=Twist()
	base_data.linear.x=s	
	base_data.angular.z=s2	
	pub.publish(base_data)
	

 
def callback(msg):
        
        print 'in callback function'
	

        print len(msg.ranges)
        
	ldata=list(msg.ranges)
 	for i in range(len(ldata)):
    		if ldata[i]==0.006000000052154064:
   			ldata[i]=10
  		if ldata[i]==0.007000000216066837:
			ldata[i]=10
		if ldata[i]==0.01899999938905239:
			ldata[i]=10
		if ldata[i]==0.00800000037997961:
			ldata[i]=10
		if ldata[i]==0.014999999664723873:
			ldata[i]=10

	bR=np.nanmin(ldata[0:len(ldata)/7])
	r=np.nanmin(ldata[len(ldata)/7:2*(len(ldata)/7)])
	fR=np.nanmin(ldata[2*len(ldata)/7:3*(len(ldata)/7)])
	f=np.nanmin(ldata[3*len(ldata)/7:4*(len(ldata)/7)])
	fL= np.nanmin(ldata[4*len(ldata)/7:5*(len(ldata)/7)])
	l= np.nanmin(ldata[5*len(ldata)/7:6*(len(ldata)/7)])
	bL=np.nanmin(ldata[6*(len(ldata)/7):len(ldata)])

	
	'''
	backRight= msg.ranges[0:len(msg.ranges)/7]
	right= msg.ranges[len(msg.ranges)/7:2*(len(msg.ranges)/7)]
	forwardR= msg.ranges[2*len(msg.ranges)/7:3*(len(msg.ranges)/7)]
	forward= msg.ranges[3*len(msg.ranges)/7:4*(len(msg.ranges)/7)]
	forwardL= msg.ranges[4*len(msg.ranges)/7:5*(len(msg.ranges)/7)]
	left= msg.ranges[5*len(msg.ranges)/7:6*(len(msg.ranges)/7)]
	backLeft= msg.ranges[6*(len(msg.ranges)/7):len(msg.ranges)]

	r=np.nansum(right)/(len(msg.ranges)/7)
	fR=np.nansum(forwardR)/(len(msg.ranges)/7)
	f=np.nansum(forward)/(len(msg.ranges)/7)
	fL=np.nansum(forwardL)/(len(msg.ranges)/7)
        l=np.nansum(left)/(len(msg.ranges)/7) '''


	print(r,fR,f, fL, l)
 	d=0.8
	d2=0.75
	backRight=-0.28
	forLeft=0.28

######################################################### if state=1 find wall
	#global state
	if 'find wall' in state :

		if fL>d and f>d and fR>0.5:
        		move(forLeft,0)
			print("forward - find wall")
   
    		elif r>0.5 and fR>0.5:
        	
			rightLeftConflict.append('r')
			del findwall [:]
			move(0,backRight)
			print("right  - find wall")	
		#else : 
		#	move(0,0)
		#	print("stop  - find wall")
    
    
		if l>d2 and l<0.85 :
        		del state [:]
        		del rightLeftConflict [:]
			del findwall [:]

######################################################### if state=0 follow the left wall
	else:	
	
        	if l>d2 and fL>d2 :
			rightLeftConflict.append('l')
			findwall.append('l')
			
			print(l)
			if findwall.count('l') >40 : #and l>0.9:
				print ('find wall find wall find wall find wall find wall')
				print (findwall.count('l'))
				state.append('find wall')
				#for i in range(3):			
				#	move(forLeft,0.2)
				#	print("left and forward")

			elif rightLeftConflict.count('r') > 3 and rightLeftConflict.count('l') > 3: 
				print(rightLeftConflict)
				for i in range(2):
					move(forLeft,-0.32)
					print("right and forward (stuck left-right)")
			
			else:
				move(0,forLeft)
				print("left")
			
		
		else:
			if  fL>d and f>d and fR>0.5:
				del rightLeftConflict [:]
				del findwall [:]

				move(forLeft,0)
				print("forward")
		
			#elif fL>d2 and f>d2 :
			#	del rightLeftConflict [:]
				
			#	move(forLeft,0)
			#	print("left and forward") 
				
			#elif f>d2 and fR>d2 :
			#	del rightLeftConflict [:]
			
			#	move(backRight,0)
			#	print("right and forward") 	
		
			else :
				if r>0.5 and fR>0.5:
					rightLeftConflict.append('r')
					del findwall [:]
	
					move(0,backRight)
					print("right")	
						
					
				else : 
					del rightLeftConflict [:]
					del findwall [:]
					for i in range(2):
						move(backRight,-0.32)
						print("right after back")
	

if __name__=='__main__':
	
	
	try:
		rightLeftConflict=[]
		findwall=[]
		state=[]
		
		pub=rospy.Publisher('cmd_vel',Twist,queue_size=100)
		rospy.init_node('Mover',anonymous=True)
		counter=0
                print('in main function, counter is') 
		print(counter) 
		counter+=1		
		rospy.Subscriber("base_scan", LaserScan, callback)
		#rotat(0.6,10)

		rospy.spin()
	except rospy.ROSInterruptException:
		pass
